import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import NavBarSignIn from '../NavBar/NavBarSignIn';
import { login } from '../Utilitaire/reducers/authAction';

export default function SignIn() {

	const history = useHistory();
	const dispatch = useDispatch();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const handleSubmit = async (e) => {
		e.preventDefault();

		dispatch(login({ email, password }));
		history.push('/Home');
		setEmail('');
		setPassword('');
	}

	return (
		<div>
			<NavBarSignIn />
			<div className="LoginForm">
				<div className="Form_box">
					<div className="Input_login">
						<form action="" onSubmit={handleSubmit}>
							<h4>Log In </h4>
							<input 
								type="text" 
								className="Input-Field" 
								placeholder="Login"
								onChange = { (e) => {
									setEmail(e.target.value);
								}}/>
							<input 
								type = "password" 
								className="Input-Field" 
								placeholder="Password"
								onChange = { (e) => {
									setPassword(e.target.value);
								}}/>
							<div className="Checkbox">
								<input type="checkbox" className="Checkbox"/>
								<span>
									Remember Passeword
								</span>
							</div>
							<button className="Submit">
								Log In
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	)
}