import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { logout } from '../Utilitaire/reducers/authAction';

export default function Logout() {
	const user = useSelector((state) => state.authReducer.user);
	const history = useHistory();
	const dispatch = useDispatch();

	if(!user) {
		return <h1> non connecte</h1>
	}

	const handleSubmit = async (e) => {
		e.preventDefault();

		dispatch(logout());
		history.push('/');		
	}

	return (
		<div className="LogoutForm">
			<button onClick={handleSubmit}>Se déconnecter</button>
		</div>
	)
}