import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import NavBarSignUp from '../NavBar/NavBarSignUp';
import { register } from '../Utilitaire/reducers/authAction';

export default function Create() {

	const history = useHistory();
	const dispatch = useDispatch();
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [login, setLogin] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const handleSubmit = async (e) => {
		e.preventDefault();

		const newUser = { firstname, lastname, login, email, password };
		dispatch(register(newUser));
		history.push('/Home');
		setFirstname('');
		setLastname('');
		setLogin('');
		setEmail('');
		setPassword('');
	}

	return (
		<div>
			<NavBarSignUp/>
			<form id="Create" onSubmit={handleSubmit}>
				<h4>Create Account</h4>

				<input 
					type="text" 
					placeholder="First Name"
					onChange = { (e) => {
						setFirstname(e.target.value);
					}}/>

				<input 
					type="text" 
					placeholder="Last Name"
					onChange = { (e) => {
						setLastname(e.target.value);
					}}/>

				<input 
					type="text" 
					placeholder="Identifiant"
					onChange = { (e) => {
						setLogin(e.target.value);
					}}/>

				<input 
					type="email" 
					name="Email" 
					placeholder="Email"
					onChange = { (e) => {
						setEmail(e.target.value);
					}}/>

				<input 
					type="text" 
					placeholder="Password" 
					maxLength="12"
					onChange = { (e) => {
						setPassword(e.target.value);
					}}/>

				<input 
					type="text" 
					placeholder="Confirm Password"
					maxLength="12"
					onChange = { (e) => {
						setPassword(e.target.value);
					}}/>

				<div className="btn_box">
					<button type="submit">
						Submit
					</button>
				</div>
			</form>
		</div>
	)
};