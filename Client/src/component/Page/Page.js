import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Post from './Post/Post';
import { isEmpty } from '../Utilitaire/Util';
import { AllPost } from '../Utilitaire/reducers/userActions';

const Page = () => {
	
	const user = useSelector((state) => state.authReducer.user);
	const posts = useSelector((state) => state.userReducer);

	const [ loadPost, setLoadPost ] = useState(true);
	const [ count, setCount ] = useState(5);

	const dispatch = useDispatch();

	const Load = () => {
		if(
			window.innerHeight + document.documentElement.scrollTop + 1 >
			document.scrollingElement.scrollHeight
		)
		setLoadPost(true);
	};

	useEffect(() => {
		if(loadPost) {
			dispatch(AllPost());
			setLoadPost(false);
		}

		window.addEventListener("scroll", Load);

		return () => {
			 window.removeEventListener("scroll", Load);
		}
	}, [loadPost]);

	return (
		<div className="Page">
			<ul>
				{!isEmpty(posts[0]) &&
					posts.map((post) => {
						return <Post post={post} key={post._id}/>
					})
				}
			</ul>
		</div>
	)
}

export default Page;