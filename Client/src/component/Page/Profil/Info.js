import React from 'react';
import { useSelector } from 'react-redux';
import { dateParser } from "../../Utilitaire/Util";
import NavBarHome from '../../NavBar/NavBarHome';

const Info = () => {
	const user = useSelector((state) => state.authReducer.user);

	if(!user) {
		return <h1> non connecte</h1>
	}

	return (
		<div id="InfoForm">
			<NavBarHome/>
			<div className="Profil">
				<h2> Welcome {user.firstname} {user.lastname}</h2>
			</div>
			<div className="Info">
				<h3> Info </h3>
				<h4>
					Prénom: {user.firstname}<br/>
					Nom: {user.lastname}<br/>
					Email: {user.email}<br/>
					Login: {user.login}<br/>
					Message: {user.messages.length}<br/>
					Abonné: {user.abonne.length}<br/>
					Abonnement: {user.abonnement.length}<br/>
					Parmi nous depuis le { dateParser (user.createdAt)}
				</h4>
			</div>
		</div>
	)
}

export default Info;