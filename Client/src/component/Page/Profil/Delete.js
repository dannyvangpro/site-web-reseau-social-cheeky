import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { deleteUser } from '../../Utilitaire/reducers/userActions';
import NavBarHome from '../../NavBar/NavBarHome';

const Delete = () => {
	const user = useSelector((state) => state.authReducer.user);
	const history = useHistory();
	const dispatch = useDispatch();

	if(!user) {
		return <h1> non connecte</h1>
	}

	const handleSubmit = (e) => {
		e.preventDefault();

		dispatch(deleteUser());
		history.push('/');		
	}

	const handleNon = (e) => {
		e.preventDefault();

		history.push('/Profil');
	}

	return (
		<div id="DeleteForm">
			<NavBarHome/>
			<div className="Delete">
				<h4>Voulez-vous vraiment supprimer votre compte ?</h4>
				<button onClick={handleSubmit}> Oui </button>
				<button onClick={handleNon}> Non </button>
			</div>
		</div>
	)
}

export default Delete;