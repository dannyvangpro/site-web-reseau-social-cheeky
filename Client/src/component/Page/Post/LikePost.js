import React, { useState, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import { LikePost, UnlikePost } from "../../Utilitaire/reducers/userActions";

const LikeAPost = ({ post }) => {

	const user = useSelector((state) => state.authReducer.user);

	const [ liked, setLiked ] = useState(false);
	const dispatch = useDispatch();

	const like = () => {
		dispatch(LikePost(post._id, user._id))
		setLiked(true);
	};
	
	const unlike = () => {
		dispatch(UnlikePost(post._id, user._id))
		setLiked(false);
	}

	useEffect(() => {
		if (post.likes.includes(user._id)) setLiked(true);
		else setLiked(false);
	}, [user._id, post.likes, liked]);

	return (
		<div className="like-container">
			{user._id && liked === false && (
				<img src="./img/icons/heart.svg" onClick={like} alt="like" />
			)}
			{user._id && liked && (
				<img src="./img/icons/heart-filled.svg" onClick={unlike} alt="unlike" />
			)}
			<span>{post.likes.length}</span>
		</div>
	);
};

export default LikeAPost;