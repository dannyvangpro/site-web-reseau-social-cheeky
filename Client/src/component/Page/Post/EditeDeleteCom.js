import React, { useState, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DeleteCom, EditeCom } from "../../Utilitaire/reducers/userActions";

const EditDeleteCom = ({ comment, postId }) => {

  const user = useSelector((state) => state.authReducer.user);

  const [ isAuthor, setIsAuthor ] = useState(false);
  const [ edit, setEdit ] = useState(false);
  const [ text, setText ] = useState("");
  const dispatch = useDispatch();

  const handleEdit = (e) => {
    e.preventDefault();

    if (text) {
      dispatch(EditeCom(postId, text, comment._id));
      setText("");
      setEdit(false);
    }
  };

  const handleDelete = () => {
    dispatch(DeleteCom(postId, comment._id));
  };

  useEffect(() => {
    const checkAuthor = () => {
      if (user._id === comment.author) {
        setIsAuthor(true);
      }
    };
    checkAuthor();
  }, [user._id, comment.author]);

  return (
    <div className="edit-comment">
      {isAuthor && edit === false && (
        <span onClick={() => setEdit(!edit)}>
          <img src="./img/icons/edit.svg" alt="edit-comment" />
        </span>
      )}
      {isAuthor && edit && (
        <form onSubmit={(e) => handleEdit(e)} className="edit-comment-form">
          <label onClick={() => setEdit(!edit)} htmlFor="text">
            Editer
          </label>
          <br />
          <input
            type="text"
            name="text"
            onChange={(e) => setText(e.target.value)}
            defaultValue={comment.text}
          />
          <br />
          <div className="btn">
            <span
              onClick={() => {
                if (window.confirm("Voulez-vous supprimer ce commentaire ?")) {
                  handleDelete();
                }
              }}
            >
              <img src="./img/icons/trash.svg" alt="trash" />
            </span>
            <input type="submit" value="Valider modification" />
          </div>
        </form>
      )}
    </div>
  );
};

export default EditDeleteCom;