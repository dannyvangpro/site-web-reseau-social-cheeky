import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AllPost } from '../../Utilitaire/reducers/userActions';
import { CreatePost } from '../../Utilitaire/reducers/userActions';
import { isEmpty, timestampParser } from '../../Utilitaire/Util';

const NewPost = () => {

	const user = useSelector((state) => state.authReducer.user);
	const poster = useSelector((state) => state.userReducer.post);
	
	const [ isLoading, setIsLoading ] = useState(true);
	const [ post, setPost ] = useState("");
	
	const history = useHistory();
	const dispatch = useDispatch();
	
	const handlePost = async () => {
		if(post) {
			await dispatch(CreatePost(post));
			cancelPost();
		}
		else {
			alert("Vous n'avez rien à dire ?");
		}
	};

	const cancelPost = () => {
		setPost('');
	};

	useEffect(() => {
		if(!isEmpty(user)) setIsLoading(false);
	}, [user, post]);

	return (
		<div id="HomeForm">
			<div className="Post">
				<h4>
					Comment allez-vous aujourd'hui {user.login}?
				</h4>
				<textarea
					name="post"
					id="post"
					cols="25"
					rows="2"
					onChange={(e) => setPost(e.target.value)}
					value={post}
					/>
				{post > 20 
					?(
						<li className="card-container">
							<div className="card-right">
								<div className="card-header">
									<div className="pseudo">
										<h3>{user.login}</h3>
									</div>
									<span>{timestampParser(Date.now())}</span>
								</div>
								<div className="content">
									<p>{post}</p>
								</div>
							</div>
						</li>
					) : null}
				<div className="footer-form">
					<div className="btn-send">
						{post > 20 ? (
							<button className="cancel" onClick={cancelPost}>
								Annuler message
							</button>
						) : null}
						<button className="send" onClick={handlePost}>
							Envoyer
						</button>
					</div>
				</div>
			</div>
		</div>
	)
}

export default NewPost;