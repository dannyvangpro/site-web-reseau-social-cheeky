import React from "react";
import { useDispatch } from "react-redux";
import { DeletePost } from '../../Utilitaire/reducers/userActions';

const DeleteAPost = (props) => {
	const dispatch = useDispatch();

	const deleteQuote = () => {
		dispatch(DeletePost(props.id));
	};

	return (
		<div
			onClick={() => {
				if (window.confirm("Voulez-vous supprimer cet article ?")) {
					deleteQuote();
				}
			}}
			>
			<img src="./img/icons/trash.svg" alt="trash" />
		</div>
	);
};

export default DeleteAPost;