import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Follow from'../Friends/Follow';
import EditDeleteCom from "./EditeDeleteCom";
import { timestampParser } from '../../Utilitaire/Util';
import { CreateCom, AllPost } from '../../Utilitaire/reducers/userActions';

const NewCom = ({ post }) => {

	const user = useSelector((state) => state.authReducer.user);

	const [ text, setText ] = useState("");
	const dispatch = useDispatch();

	const handleComment = (e) => {
		e.preventDefault();

		if (text) {
			dispatch(CreateCom(post._id, text, user._id))
			.then(() => dispatch(AllPost()))
			.then(() => setText(""));
		}
	};

	return (
		<div className="comments-container">
			{post.comments.map((comment) => {
				return (
					<div className={comment.author === user._id ? "comment-container client" : "comment-container" } key={comment._id}>
						<div className="right-part">
							<div className="comment-header">
								<div className="pseudo">
									<h3>{comment.authorLogin}</h3>
									{comment.author !== user._id && (
										<Follow userid={comment.author} type={"card"} />
									)}
								</div>
								<span>{timestampParser(comment.timestamps)}</span>
							</div>
							<p>{comment.text}</p>
							<EditDeleteCom comment={comment} postId={post._id} />
						</div>
					</div>
				);
			})}
			{user._id && (
				<form onSubmit={handleComment} className="comment-form">
					<input
						type="text"
						name="text"
						onChange={(e) => setText(e.target.value)}
						value={text}
						placeholder="Laissez un commentaire"
						/>
					<br />
					<input type="submit" value="Envoyer" />
				</form>
			)}
		</div>
	);
};

export default NewCom;