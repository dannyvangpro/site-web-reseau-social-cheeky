import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";

import NewCom from './NewCom';
import LikeAPost from './LikePost';
import Follow from'../Friends/Follow';
import DeleteAPost from './DeletePost';

import { isEmpty, dateParser } from '../../Utilitaire/Util';
import { AllPost, UpdatePost } from '../../Utilitaire/reducers/userActions';

const Post = ({ post }) => {

	const userdata = useSelector((state) => state.authReducer.user);
	const alluser = useSelector((state) => state.allusersReducer);

	const [ isUpdated, setIsUpdated ] = useState(false);
	const [ updatePost, setUpdatePost ] = useState(null);
	const [ showComments, setShowComments ] = useState(false);

	const dispatch = useDispatch();

	const handleUpdate = async () => {
		if(updatePost) {
			await dispatch(UpdatePost(updatePost)).then(() => {
				dispatch(AllPost());
			})
		}
	};

	return (
		<li className="card-container" key={post._id}>
			<div className="card-right">
				<div className="card-header">
					<div className="pseudo">
						<h3>
							{!isEmpty(alluser) &&
							alluser.map((user) => {
								if (user._id === post.author._id) return user.login;
							})}
						</h3>
						{post.author._id !== userdata._id && (
							<Follow userid={post.author._id} type={"card"} />
						)}
					</div>
					<span>{dateParser(post.createdAt)}</span>
				</div>
				{isUpdated === false && <p>{post.post}</p>}
				{isUpdated && (
					<div className="update-post">
						<textarea
							defaultValue={post.post}
							onChange={(e) => setUpdatePost(e.target.value)}
							/>
						<div className="button-container">
							<button className="btn" onClick={handleUpdate}>
								Valider modification
							</button>
						</div>
					</div>
				)}
				{userdata._id === post.author._id && (
					<div className="button-container">
						<div onClick={() => setIsUpdated(true)}>
							{" "}
							<img src="./img/icons/edit.svg" alt="edit-comment" />
						</div>
						<DeleteAPost id={post._id} />
					</div>
				)}
				<div className="card-footer">
					<div className="comment-icon">
						<img
							src="./img/icons/message1.svg"
							onClick={() => setShowComments(!showComments)}
							alt="comment"
							/>
						<span>{post.comments.length}</span>
					</div>
					<LikeAPost post={post} />
				</div>
				{showComments && <NewCom post={post} />}
			</div>
		</li>
	)
}

export default Post;