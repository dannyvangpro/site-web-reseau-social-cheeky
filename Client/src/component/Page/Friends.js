import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import NavBarHome from '../NavBar/NavBarHome';

import { isEmpty } from '../Utilitaire/Util';
import { getAllUsers } from '../Utilitaire/reducers/allusersAction';

import Follow from './Friends/Follow';

export default function Friends() {
	
	const user = useSelector((state) => state.authReducer.user);
	const alluser = useSelector((state) => state.allusersReducer.payload);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getAllUsers());
	});

	return (
		<div>
			<NavBarHome/>
			<h2>Liste des amis</h2>
			{!isEmpty(alluser) && alluser.map((friend) => {
				if(user._id !== friend._id){
					return (
						<ul>
							<h3> {friend.login}</h3>
							<div className="follow-handler">
								<Follow
									userid={friend._id}
									type={"suggestion"}
									/>
							</div>
						</ul>
					)
				}
			})}
		</div>
	)
}

