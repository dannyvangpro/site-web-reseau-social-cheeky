import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { followUser, unfollowUser } from '../../Utilitaire/reducers/userActions';
import { isEmpty } from '../../Utilitaire/Util';

const Follow = ({ userid, type }) => {

	const user = useSelector((state) => state.authReducer.user);

	const [ isFollowed, setIsFollowed ] = useState(false);

	const dispatch = useDispatch();

	const handleFollow = () => {
		dispatch(followUser(userid));
		setIsFollowed(true);
	};

	const handleUnfollow = () => {
		dispatch(unfollowUser(userid));
		setIsFollowed(false);
	};

	useEffect(() => {
		if(!isEmpty(user.abonnement)) {
			if(user.abonnement.includes(userid)) {
				setIsFollowed(true);
			}
			else {
				setIsFollowed(false);
			}
		}
	}, [user, userid]);

	return (
		<div>
			{!isEmpty(user) && isFollowed && (
				<span onClick={handleUnfollow}>
					{type === "suggestion" && (
						<button className="unfollow-btn">
							Abonné
						</button>
					)}
					{type === "card" && (
						<img src="./img/icons/checked.svg" alt="checked" />
					)}
				</span>
			)}
			{!isEmpty(user) && isFollowed === false && (
				<span onClick={handleFollow}>
					{type === "suggestion" && (
						<button className="follow-btn">
							Suivre
						</button>
					)}
					{type === "card" && (
						<img src="./img/icons/check.svg" alt="check" />
					)}
				</span>
			)}
		</div>
	)
}

export default Follow;