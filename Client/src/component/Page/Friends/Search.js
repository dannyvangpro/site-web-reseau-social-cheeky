import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Axios from 'axios';

import { isEmpty } from '../../Utilitaire/Util';

const Search = () => {
	const user = useSelector((state) => state.authReducer.user);

	const [ firstname, setFirstname ] = useState("");

	const onSubmit = (e) => {
		e.preventDefault();

		if(firstname === "" || firstname === null) {
			alert("Entrer un prenom");
		}
		else {
			Axios({
				method: "GET",
				url: "http://localhost:4000/api/friend/search",
				headers: {
					Authorization: "Bearer " +localStorage.getItem("cookie")
				},
				body: {
					firstname: firstname
				}
			})
			.then(res => {
				console.log(res)
			})
			.catch(err => {
				console.log(err)
			})
		}
		
	};

	return (
		<div>
			<form className="search">
				<textarea
					type="text"
					onChange={(e) => setFirstname(e.target.value)}
					/>
				<button className="search_btn" onClick={onSubmit}>
					Search a user
				</button>
			</form>
		</div>
	)
};

export default Search;