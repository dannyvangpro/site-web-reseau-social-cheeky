import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Axios from 'axios';

import NewCom from './Post/NewCom';
import LikeAPost from './Post/LikePost';
import DeleteAPost from './Post/DeletePost';

import NavBarProfil from '../NavBar/NavBarProfil';
import { updateBio } from '../Utilitaire/reducers/userActions';
import { isEmpty, dateParser } from '../Utilitaire/Util';


const Profil = () => {

	const user = useSelector((state) => state.authReducer.user);
	const posts = useSelector((state) => state.authReducer.posts);

	const [ bio, setBio ] = useState('');
	const [ updateForm, setUpdateForm ] = useState(false);
	const [ showComments, setShowComments ] = useState(false);

	const history = useHistory();
	const dispatch = useDispatch();

	const handleUpdate = () => {
		dispatch(updateBio(bio));
		setUpdateForm(false);
	}

	const Delete = () => {
		history.push('/Delete');
	};

	Axios({
		method: "GET",
		url: "http://localhost:4000/api/post/userspost",
		headers: {
			Authorization: "Bearer "+localStorage.getItem("cookie")
		}
	});

	return (
		<div id="ProfilForm">
			<NavBarProfil/>
			<div className="Profil">
				<h2> Welcome {user.firstname} {user.lastname}</h2>
			</div>
			<div className="UpdateBio">
				<h3>Bio</h3>
				<div className="textarea">
					{updateForm === false && (
						<>
							<p onClick={() => setUpdateForm(!updateForm)} >{user.bio}</p>
							<button onClick={() => setUpdateForm(!updateForm)}>Modifier</button>
						</>
					)}
					{updateForm && (
						<>
							<textarea 
								type="text" 
								defaultValue={user.bio} 
								onChange={(e) => setBio(e.target.value)}>
							</textarea>
							<button onClick ={handleUpdate}>Ok ?</button>
						</>
					)}
				</div>
			</div>

			<div className="UL">
				<h3> Mes posts </h3>
				<ul>
					{!isEmpty(posts[0]) &&
						posts.map((post) => {
							if(post.author._id === user._id){
								return (
									<ul> 
										<li>
											<div className="Header">
												<div className="UserLogin">
													{user.login}<br/>
												</div>
												<span>{dateParser(post.createdAt)}</span>
											</div>
											<div className="UserPost">
												{post.post}<br/>
											</div>
											<div className="footer">
												<DeleteAPost id={post._id} />
												<div className="card-footer">
													<div className="comment-icon">
														<img
															src="./img/icons/message1.svg"
															onClick={() => setShowComments(!showComments)}
															alt="comment"
															/>
														<span>{post.comments.length}</span>
													</div>
													<LikeAPost post={post}/>
												</div>
												{showComments && <NewCom post={post} />}
											</div>
										</li>
									</ul>
								)
							}
						})
					}
				</ul>
			</div>
			<div className="Delete">
				Supprimer son compte 
				<button onClick={Delete}>
					Supprimer ?
				</button>
			</div>
		</div>
	)
}

export default Profil;