import React from 'react';
import { useSelector } from 'react-redux';

import NavBarHome from '../NavBar/NavBarHome';

import NewPost from './Post/NewPost';
import Post from './Post/Post';

import { isEmpty } from '../Utilitaire/Util';


const Home = () => {

	const users = useSelector((state) => state.authReducer.user);
	const posts = useSelector((state) => state.userReducer.posts);

	return (
		<div id="HomeForm">
			<NavBarHome/>
			<div className="Post">
				<NewPost/>
			</div>
			<div className="ULHome">
				<ul>
					{!isEmpty(posts[0]) && posts.map((post) => {
						return <Post post={post} key={post._id} />;
					})}
				</ul>
			</div>
		</div>
	)
}

export default Home;