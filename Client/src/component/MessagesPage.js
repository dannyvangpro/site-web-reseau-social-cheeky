import React from 'react'
import NavBarHome from './NavBar/NavBarHome'

export default class MessagesHome extends React.Component {
	render() {
		return <div>
			<NavBarHome/>
			<h2>Liste des messages</h2>
		</div>
	}
}