import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import { getUser } from "./Utilitaire/reducers/authAction";

import MessagesPage from './MessagesPage';
import Footer from './NavBar/Footer'

import Acceuil from './Page/Acceuil';
import Contact from './Page/Contact';
import Friends from './Page/Friends';
import Home from './Page/Home';

import Profil from './Page/Profil';
import Info from './Page/Profil/Info';
import Delete from './Page/Profil/Delete';

import Logout from './register/Logout';
import SignIn from './register/SignIn';
import SignUp from './register/SignUp';

import PrivateRoute from './Private/PrivateRoute';


export default function NavigationPanel() {

	const dispatch = useDispatch();
	const getAuth = () => dispatch(getUser());

	useEffect(() => {
		getAuth();
	}, []);
		
	return (
		<BrowserRouter>
			<Route exact path='/' component={Acceuil}/>
			<PrivateRoute exact path='/Home' component={Home}/>
			<Route exact path='/SignIn' component={SignIn}/>
			<Route exact path='/SignUp' component={SignUp}/>
			<Route exact path='/Logout' component={Logout}/>
			<Route exact path='/Contact' component={Contact}/>
			<PrivateRoute exact path='/Messages' component={MessagesPage}/>
			<PrivateRoute exact path='/Profil' component={Profil}/>
			<PrivateRoute exact path='/Info' component={Info}/>
			<PrivateRoute exact path='/Delete' component={Delete}/>
			<PrivateRoute exact path='/Friends' component={Friends}/>
			<Footer/>
		</BrowserRouter>
	)
}