import React from 'react';

export default class Footer extends React.Component{

	render(){

		return <div id="Footer">
			<p>
    			Copyright &copy; BOUBRIK & VANG 2021
    		</p>
    	</div>
	}
}