import React from 'react';
import Logo from "../img/logo.png";
import Menu from "../img/menu-alt.png";
import { Link } from 'react-router-dom'

export default class NavBarContact extends React.Component{

	render(){

		return <div id="NavBar">
			<div>
				<Link className="Logo" to='/'>
					<img src={Logo} width="70" heigth="70" alt="" />
				</Link>
				<Link className="Titre" to='/'>
					<h1>Cheeky</h1>
				</Link>
				<div className="Menu">
					<ul>
						<li className="Active">
							<a href="#">
								<img src={Menu} alt=""/>
							</a>
							<ul className="MenuCRS">
								<li className="CRS">
									<Link to='/SignIn'>
										Sign In
									</Link>
								</li>
								<li className="CRS">
									<Link to='/SignUp'>
										Sign Up
									</Link>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	}
}