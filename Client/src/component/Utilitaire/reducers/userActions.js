import Axios from 'axios';

export const USER = "User";
export const UPDATEBIO = "Update bio";
export const DELETEUSER = "Delete user";
export const FOLLOWUSER = "Follow user";
export const UNFOLLOWUSER = "Unfollow user";
export const CREATEPOST = "Create post";
export const ALLPOST = "All post";
export const FOLLOWINGPOST = "Following post";
export const USERPOST = "User post";
export const UPDATEPOST = "Update post";
export const DELETEPOST = "Delete post";
export const LIKEPOST = "Like post";
export const UNLIKEPOST = "Unlike post";
export const CREATECOM = "Create com";
export const UPDATECOM = "Update com";
export const DELETECOM ="Delete com";


export const getUser = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get('http://localhost:4000/api/user/', config)
		dispatch({ 
			type: "User", 
			payload: res.data 
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const updateBio = (bio) => async dispatch => {
	try { 
		const res = await Axios({
			method: "PUT",
			url: 'http://localhost:4000/api/user/update',
			headers: {
				Authorization: "Bearer " +localStorage.getItem("cookie")
			},
			data: {bio}
		})
		dispatch({ 
			type: "Update bio", 
			payload: res.data.user.bio
		});
	}catch(err) {
		console.log( err )
	};
};

export const deleteUser = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.setItem('cookie', '')
			}
		};

		const res = await Axios.delete(
			'http://localhost:4000/api/user/delete',
			config
		)
		dispatch({ 
			type: "Delete user", 
			payload: res.data
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
}

export const followUser = (userid) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			'http://localhost:4000/api/friend/follow',
			userid,
			config
		)
		dispatch({ 
			type: "Follow user",
			payload: userid
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const unfollowUser = (userid) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			'http://localhost:4000/api/friend/unfollow',
			userid,
			config
		)
		dispatch({ 
			type: "Unfollow user",
			payload: userid
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const CreatePost = (post) => async dispatch => {
	try { 
		const res = await Axios({
			method: "POST",
			url: 'http://localhost:4000/api/post/post',
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			},
			data: {post}
		});
		dispatch({ 
			type: "Create post",
			payload: ''
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const AllPost = () => {
	return (dispatch) => {
		return Axios({
			method: "GET",
			url: 'http://localhost:4000/api/post/',
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		})
		.then((res) => {
			dispatch({
				type: "All post",
				payload: res.data
			})
		})
		.catch((err) => {
			console.log(err)
		});
	};
};

export const FollowingPost = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get(
			'http://localhost:4000/api/post/followingpost',
			config
		)
		dispatch({ 
			type: "Following post",
			payload: res.data
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const UserPost = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get(
			'http://localhost:4000/api/post/userspost',
			config
		)
		dispatch({ 
			type: "User post",
			payload: res.data
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const UpdatePost = (postid, post) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.put(
			`http://localhost:4000/api/post/update/${postid}`,
			post,
			config
		)
		dispatch({ 
			type: "Update post",
			payload: { post, postid }
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const DeletePost = (postId) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.delete(
			`http://localhost:4000/api/post/delete/${postId}`,
			config
		)
		dispatch({ 
			type: "Delete post",
			payload: { postId }
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const LikePost = (postId, id) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			"http://localhost:4000/api/post/like",
			postId,
			id,
			config
		)
		dispatch({ 
			type: "Like post",
			payload: {postId}
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const UnlikePost = (postId, id) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			"http://localhost:4000/api/post/unlike",
			postId,
			id,
			config
		)
		dispatch({ 
			type: "Unlike post",
			payload: {postId}
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const CreateCom = (postId, text, id) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			"http://localhost:4000/api/post/com",
			postId,
			text,
			id,
			config
		)
		dispatch({ 
			type: "Create com",
			payload: {postId}
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const EditeCom = (postId, text, comId) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.patch(
			`http://localhost:4000/api/post/edit_com/${postId}`,
			comId,
			text,
			config
		)
		dispatch({ 
			type: "Update com",
			payload: {
				postId,
				comId,
				text
			}
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const DeleteCom = (postId, comId) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.delete(
			`http://localhost:4000/api/post/delete_com/${postId}`,
			comId,
			config
		)
		dispatch({ 
			type: "Delete com",
			payload: {
				postId,
				comId
			}
		});
		console.log(res.data); 
	}catch(err) {
		console.log( err )
	};
};

export const CreateMessage = (message) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.post(
			'http://localhost:4000/api/friend/message',
			message,
			config
		)
		dispatch({ 
			type: "Message",
			payload: message
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const AllMessage = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get(
			'http://localhost:4000/api/friend/all_messages',
			config
		)
		dispatch({ 
			type: "All message",
			payload: res.data
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const AllMessageUser = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get(
			'http://localhost:4000/api/friend/all_message_user',
			config
		)
		dispatch({ 
			type: "All message user",
			payload: res.data
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};

export const DeleteMessage = (messageId) => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.post(
			'http://localhost:4000/api/friend/delete',
			messageId,
			config
		)
		dispatch({ 
			type: "Delete message",
			payload: messageId
		}); 
		console.log(res.data);
	}catch(err) {
		console.log( err )
	};
};