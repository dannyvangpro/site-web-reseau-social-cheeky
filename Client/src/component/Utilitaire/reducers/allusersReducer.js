import { ALLUSER } from "./allusersAction";

const initialState = {};

const allusersReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case ALLUSER:
			return {
				...state,
				payload,
			};
		default:
			return state;
	}
};

export default allusersReducer;