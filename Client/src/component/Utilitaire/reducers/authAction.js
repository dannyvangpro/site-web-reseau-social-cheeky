import Axios from 'axios';

export const UserLoading = "User loading";
export const User = "User";
export const Register = "Register user";
export const Login = "Login user";
export const Logout = "Logout user";

const userLoading = (dispatch) => {
	dispatch({
		type: "User loading"
	})
}

export const register = (formData) => {
	return (dispatch) => {
		return Axios.post(
			'http://localhost:4000/api/sign/register',
			formData
		)
		.then((res) => {
			localStorage.setItem("cookie", res.data.cookies);
			localStorage.setItem("user", res.data.user);
			dispatch({
				type: "Register user",
				payload: res.data
			})
		})
		.catch((err) => {
			console.dir(err);
		})
	}
};

export const login = (formData) => {
	return (dispatch) => {
		return Axios.post(
			'http://localhost:4000/api/sign/login',
			formData
		)
		.then((res) => {
			localStorage.setItem("cookie", res.data.cookies);
			localStorage.setItem("user", res.data.user);
			dispatch({
				type: "Login user",
				payload: res.data
			})
		})
		.catch((err) => {
			console.dir(err);
		})
	}
};

export const logout = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.setItem('cookie', '')
			}
		};

		const res = await Axios.post('http://localhost:4000/api/sign/logout', config)
		dispatch({ 
			type: "Logout user", 
			payload: res.data 
		}); 
	}catch(err) {
		console.log( err )
	};
}

export const getUser = () => async dispatch => {
	try { 
		const config = {
			headers: {
				Authorization: 'Bearer ' + localStorage.getItem('cookie')
			}
		};

		const res = await Axios.get('http://localhost:4000/api/user/', config)
		dispatch({ 
			type: "User", 
			payload: res.data 
		}); 
	}catch(err) {
		console.log( err )
	};
};