import { combineReducers } from 'redux';
import authReducer from './authReducer';
import userReducer from './userReducer';
import allusersReducer from './allusersReducer';

export default combineReducers ({
	authReducer,
	userReducer,
	allusersReducer
}) ;