import {
	USER,
	UPDATEBIO,
	DELETEUSER,
	FOLLOWUSER,
	UNFOLLOWUSER,
	ALLPOST,
	FOLLOWINGPOST,
	USERPOST,
	UPDATEPOST,
	DELETEPOST,
	LIKEPOST,
	UNLIKEPOST,
	UPDATECOM,
	DELETECOM
} from "./userActions";

const initialState = {
	cookie: localStorage.getItem("cookie"),
	user: null,
	auth: false,
	loading: false,
	post: null
};

const userReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case USER:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case UPDATEBIO:
			return {
				...state,
				loading: false,
				auth: true,
				bio: payload,
			};
		case DELETEUSER:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case FOLLOWUSER:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case UNFOLLOWUSER:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case ALLPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case FOLLOWINGPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case USERPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case UPDATEPOST:
			return {
				...state,
				loading: false,
				auth: true,
				post: payload,
			};
		case DELETEPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case LIKEPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case UNLIKEPOST:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case UPDATECOM:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case DELETECOM:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		default:
			return state;
	}
};

export default userReducer ;