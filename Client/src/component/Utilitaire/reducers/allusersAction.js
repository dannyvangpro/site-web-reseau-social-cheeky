import Axios from 'axios';

export const ALLUSER = "All users";

export const getAllUsers = () => {
	return(dispatch) => { //dispatch c'est ce qu'on va envoyer au reducer
		return Axios({
			method: "GET",
			url: 'http://localhost:4000/api/user/all',
			headers: {
				Authorization: "Bearer "+localStorage.getItem("cookie")
			}
		})
		.then((res) => {
			dispatch({ 
				type: "All users", 
				payload: res.data.Users
			});
		})
		.catch((err) => console.log( err ));
	};
};