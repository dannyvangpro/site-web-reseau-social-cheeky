import {
	UserLoading,
	User,
	Register,
	Login,
	Logout
} from "./authAction";

const initialState = {
	cookie: localStorage.getItem("cookie"),
	user: null,
	auth: false,
	loading: false
};

const authReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case UserLoading:
			return {
				...state,
				loading: true,
			};
		case Register:
			localStorage.setItem("cookie", payload.cookie);
			localStorage.setItem("user", payload.user);
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case Login:
			localStorage.setItem("cookie", payload.cookie);
			localStorage.setItem("user", payload.user);
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case User:
			return {
				...state,
				loading: false,
				auth: true,
				...payload,
			};
		case Logout:
			return {
				...state,
				token: null,
				auth: false,
				user: null,
				loading: false,
			};
		default:
			return state;
	}
};

export default authReducer ;