import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

 const PrivateRoute = ({ component: Component, ...rest }) => {
	const isAuth = useSelector(state => state.authReducer.auth);

	if(!isAuth) {
		return <Redirect to='/'/>
	}

	return (
		<div>
			<Route component= { Component } { ...rest }/>
		</div>
	)
};

export default PrivateRoute;