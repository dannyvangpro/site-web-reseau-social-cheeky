const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require("mongoose");
const cors = require("cors");
const CookieParser = require("cookie-parser");
const session = require("express-session");

//Import routes
const loginRoute = require("./routes/Login");
const userRoute = require("./routes/User");
const friendRoute = require("./routes/Friend");
const postRoute = require("./routes/Post");

dotenv.config();

//Cookie
app.use(CookieParser());

//Connect to db
mongoose.connect(
	process.env.DB_CONNECT,
	{ 
		useNewUrlParser: true, 
		useUnifiedTopology: true,
		useCreateIndex: true,
		useFindAndModify: false
	}
)
.then(() => console.log('Connected to db !'))
.catch((err) => console.log('Error', err));

//Middleware
app.use(express());
app.use(express.json());

app.use(cors({
	credentials: true,
	origin: ['http://localhost:3000', 'http://localhost:4000'],
	'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
	'preflightContinue': false
}));

app.use(session({
	key: "userId",
	secret: "subscribe",
	resave: false,
	saveUninitialized: false,
	cookie: {
		expires: 60 * 60 * 24,
	},
}));

//Route middleware
app.use('/api/sign', loginRoute);
app.use('/api/user', userRoute);
app.use('/api/friend', friendRoute);
app.use('/api/post', postRoute);

app.listen(4000, () => console.log('Server up and running'));