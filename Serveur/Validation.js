//Validation
const Joi = require("@hapi/joi");

const RegisterValide = (data) => {
	const Schema = Joi.object({
		firstname: Joi.string()
			.min(3)
			.required(),
		lastname: Joi.string()
			.min(3)
			.required(),
		login: Joi.string()
			.min(3)
			.max(15)
			.required(),
		email: Joi.string()
			.min(6)
			.required()
			.email(),
		password: Joi
			.string()
			.min(6)
			.max(15)
			.required()
	});

	//Validation des données
	return Schema.validate(data);
};

const LoginValide = (data) => {
	const Schema = Joi.object({
		email: Joi.string()
			.required()
			.email(),
		password: Joi.string()
			.required()
	});

	//Validation des données
	return Schema.validate(data);
};

const PostValide = (data) => {
	const Schema = Joi.object({
		post: Joi.string()
			.required()
	});

	//Validation des données
	return Schema.validate(data);
};

const MessageValide = (data) => {
	const Schema = Joi.object({
		message: Joi.string()
			.required()
	});

	//Validation des données
	return Schema.validate(data);
};

module.exports.RegisterValide = RegisterValide;
module.exports.LoginValide = LoginValide;
module.exports.PostValide = PostValide;
module.exports.MessageValide = MessageValide;