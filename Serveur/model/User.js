const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema.Types;

const userSchema = new mongoose.Schema(
	{
		firstname:{
			type: String,
			required: true,
			max: 255,
			min: 3
		},
		lastname: {
			type: String,
			required: true,
			max: 255,
			min: 3
		},
		login: {
			type: String,
			required: true,
			max: 15,
			min: 3
		},
		email: {
			type: String,
			required: true,
			lowercase: true,
			unique: true,
			trim: true
		},
		password: {
			type: String,
			required: true,
			max: 15,
			min: 6
		},
		abonnement: {
			type: [ObjectId],
			ref: "User"
		},
		abonne: {
			type: [ObjectId],
			ref: "User"
		},
		messages: {
			type: [
			{
				author: {
					type: [ObjectId],
					ref: "User"
				},
				message: String,
				timestamps: Number
			}],
		},
		bio: {
			type: String,
			max: 140,
		},
		createdAt: String
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("User", userSchema);