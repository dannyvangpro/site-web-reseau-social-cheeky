const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema.Types;

const postSchema = new mongoose.Schema(
	{
		author: {
			type: ObjectId,
			ref: 'User'
		},
		post: {
			type: String,
			trim: true,
			max: 255
		},
		likes: {
			type: [ObjectId],
			ref: 'User'
		},
		comments:{
			type: [
			{
				author: {
					type: [ObjectId],
					ref: "User"
				},
				authorLogin: String,
				text: String,
				timestamps: Number
			}],
			required: true
		},
		createdAt: String
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model('Post', postSchema);