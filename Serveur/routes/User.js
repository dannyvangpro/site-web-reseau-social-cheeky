const router = require("express").Router();
const ObjectID = require('mongoose').Types.ObjectId;
const User = require("../model/User");
const Post = require("../model/Post");
const { ValidateCookie } = require("./Cookie");

// Get the user
router.get('/', ValidateCookie, (req, res) => {

	User.findOne({ _id: req.user._id })
	.select('-password')
	.then(user => {
		Post.find({ author: req.user._id })
		.populate( "author", "_id name" )
		.exec((err, posts) => {
			if(err) {
				return res.status(422).json({ error: err })
			}
			res.status(200).json({ user, posts })
		})
	})
	.catch(err => {
		res.status(404).json({ error: "Error finding user" });
	});
});

// Get all the users
router.get('/all', ValidateCookie, (req, res) => {

	User.find().select('-password')
	.then(user => {
		res.status(200).json({ Users: user});
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the users" });
	});
});

// Update the user => modify the user
router.put('/update', ValidateCookie, async (req, res) => {

	const id_user = req.user._id;
	const user = await User.findById(id_user).select('firstname').select('lastname').select('-_id');

	if(!req.body) {
		res.status(400).json({ error: "No data to update" });
	}

	User.findByIdAndUpdate(
		id_user,
		{ 
			$set : { 
				bio: req.body.bio,
			},
		},
		{ 
			new: true,
			upsert: true,
			setDefaultOnInsert: true
		}
	).then(data => {
		if(!data) {
			res.status(404).json({ error: "Cannot update user " + user + " info "});
		}
		else {
			res.status(200).json(data);
		}
	}).catch(err => {
		res.status(500).json({ error: "Error finding the user !" });
	});
});

// Delete the user
router.delete('/delete', ValidateCookie, async (req, res) => {

	const id = req.user._id;
	const user = await User.findById(id).select('firstname').select('lastname').select('-_id');

	User.findByIdAndDelete(id)
	.then(data => {
		if(!data) {
			res.status(404).send({ error: "Cannot delete user " + user + " try again later !" });
		}
		else {
			res.cookie('cookie', '', { maxAge: 0 });
			res.send(data);
		}
	})
	.catch(err => {
		res.status(500).send({ error: "Error finding the user !" });
	});
});

module.exports = router;