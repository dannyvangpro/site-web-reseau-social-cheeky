const router = require("express").Router();
const ObjectID = require('mongoose').Types.ObjectId;
const User = require("../model/User");
const Post = require("../model/Post");
const { ValidateCookie } = require("./Cookie");
const { PostValide } = require("../Validation");

// Create a new post
router.post('/post', ValidateCookie, (req, res) => {
	
	//Validation des données
	const { error } = PostValide(req.body);
	if(error) return res.status(400).json({ error: error.details[0].message });

	// req.user.password 
	const the_post = req.body.post;

	const post = new Post({
		author: req.user._id,
		post: the_post,
	});

	post.save()
	.then(savePost => {
		res.status(200).json({ post: savePost});
	})
	.catch(err => {
		res.status(500).json({ error: "Error creating the post" + post });
	});
});

// Get all the post of the bd
router.get('/', ValidateCookie, (req, res) => {
	
	Post.find()
	.populate("author", "_id name")
	.populate("comments.author")
	.sort( '-createdAt')
	.then(docs => {
		if(!docs) {
			res.status.json({ errors: "Error to get all the posts" });
		}
		else {
			res.json({ post: docs });
		}
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the posts" });
	});
});

// Get all the post of the user following
router.get('/followingpost', ValidateCookie, (req, res) => {
	
	Post.find({ author: {$in: req.user.abonnement }})
	.populate("author", "_id name")
	.populate("comments.author")
	.sort( '-createdAt' )
	.then(docs => {
		if(!docs) {
			res.status.json({ errors: "Error to get all the posts" });
		}
		else {
			res.json({ post: docs });
		}
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the posts" });
	});
});

// Get all the post of the user
router.get('/userspost', ValidateCookie, (req, res) => {
	
	Post.find({ author: req.user._id })
	.populate("author", "_id name")
	.populate("comments.author")
	.sort( '-createdAt' )
	.select( '-author' )
	.then(docs => {
		if(!docs) {
			res.status.json({ errors: "Error to get all the posts" });
		}
		else {
			res.json({ post: docs });
		}
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the posts" });
	});
});

// Update a post => modify a post
router.put('/update/:postid', ValidateCookie, async (req, res) => {
	
	//Validation des données
	const { error } = PostValide(req.body);
	if(error) return res.status(400).json({ error: error.details[0].message });

	Post.findByIdAndUpdate(
		req.params.postid,
		{ 
			$set : { 
				post: req.body.post,
			},
		},
		{
			new: true,
		}
	).then(post => {
		if(!result) {
			res.status(404).json({ error: "Cannot update the post " + post });
		}
		else {
			res.status(200).json({ message: "The post " + post.post + " was edited successfully !" });
		}
	}).catch(err => {
		res.status(500).json({ error: "Error finding the post !" });
	});
});

// Delete a post
router.delete('/delete/:postId', ValidateCookie, (req, res) => {
	
	Post.findOne({ _id : req.params.postId})
	.populate( "author", "_id")
	.exec((err, post) => {
		if(err || !post) {
			return res.status(422).json({ error: "Error finding the post !" })
		}
		if(post.author._id.toString() === req.user._id.toString()) {
			post.remove()
			.then(result => {
				res.status(200).json({ message: "The post" + result + " was successfully deleted" })
			})
			.catch(err => {
				res.status(500).json({ error: err })
			})
		}
	})
});

// Like a post
router.patch('/like', ValidateCookie, async (req, res) => {
	
	//Validation des données
	// const { error } = PostValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	Post.findByIdAndUpdate(
		req.body.postId,
		{
			$addToSet: {
				likes: req.body.id,
			},
		},
		{
			new: true,
		}
	).then(post => {
		if(!post) {
			res.status(404).json({ error: "Cannot add likes to the post " + post });
		}
		else {
			User.findById(req.body.id)
			.then(user => {
				res.status(200).json({ message: "The user " + user + " liked your post " + post });
			}).catch(err => {
				res.status(500).json(err);
			});
		}
	}).catch(err => {
		res.status(500).json({ error: "Error finding the post !"+err });
	});
});

// Unlike a post
router.patch('/unlike', ValidateCookie, async (req, res) =>  {
	
	//Validation des données
	// const { error } = PostValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	Post.findByIdAndUpdate(
		req.body.postId,
		{
			$pull: {
				likes: req.body.id,
			},
		},
		{ 
			new: true,
		}
	).then(post => {
		if(!post) {
			res.status(404).json({ error: "Cannot remove the like from the post " + post });
		}
		else {
			User.findById(req.body.id)
			.then(user => {
				res.status(200).json({ message: "The user " + user + " unliked your post " + post });
			}).catch(err => {
				res.status(500).json(err);
			});
		}
	}).catch(err => {
		res.status(500).json({ error: "Error finding the post !" });
	});
});

// Create a comment
router.patch('/com', ValidateCookie, async (req, res) => {

	//Validation des données
	// const { error } = PostValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	const user = await User.findById(req.body.id);

	Post.findByIdAndUpdate(
		req.body.postId,
		{
			$push: {
				comments: {
					author: user._id,
					authorLogin: user.login,
					text: req.body.text,
					timestamp: new Date().getTime()
				}
			},
		},
		{ 
			new: true
		}
	).then(post => {
		if(!post) {
			res.status(404).json({ error: "Cannot create a comment to the post " + post });
		}
		else {
			res.status(200).json({ message: "The comment was successfully add !" + post });
		}
	}).catch(err => {
		res.status(500).json({ error: "Error creating the comment !" + err });
	});
});

// Update a comment => modify a comment
router.patch('/edit_com/:postId', ValidateCookie, async (req, res) => {

	//Validation des données
	// const { error } = PostValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	try{
		return Post.findById(req.params.postId, (err, res) => {
			const com = res.comments.find((comment) => {
				comment._id.equals(req.body.comId)
			});

			if(!com) {
				res.status(404).json({ error: "Cannot edited the comment " + com });
			}

			com.text = req.body.text;

			return res.save((err) => {
				if(!err) {
					res.status(200).json({ message: "The comment was successfully add !" + com });
				}
				res.status(500).json({ error: "Error finding and editing the comment !" + err });
			})
		})
	}catch(err) {
		res.status(500).json({ error: "Error finding and editing the comment !" + err });
	};

});

// Delete a comment
router.delete('/delete_com/:postId', ValidateCookie, async (req, res) => {

	//Validation des données
	// const { error } = PostValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	Post.findByIdAndUpdate(
		req.params.postId,
		{
			$pull: {
				comments: {
					_id: req.body.comId,
				}
			},
		},
		{ 
			new: true
		}
	).then(com => {
		if(!com) {
			res.status(404).send({ error: "Cannot delete the comment on the post " + com });
		}
		else {
			res.send({ message: "The comment " + com + " was deleted successfully !" });
		}
	})
	.catch(err => {
		res.status(500).send({ error: "Error finding and deleting the comment !" });
	});
});

module.exports = router;