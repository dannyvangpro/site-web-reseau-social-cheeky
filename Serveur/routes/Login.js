const router = require("express").Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/User");
const { RegisterValide, LoginValide } = require("../Validation");
const { CreateCookie, ValidateCookie } = require("./Cookie");


//Register
router.post('/register', (req, res) => {

	//Validation des données
	const { error } = RegisterValide(req.body);
	if(error) return res.status(422).json({ error: error.details[0].message });

	
	User.findOne({ email: req.body.email })
	.then((savedUSer) => {
		if(savedUSer) {
			res.status(422).json({ error: "User already exists with that email !"})
		}
		bcrypt.hash(req.body.password, 12)
		.then(hashedpassword => {
			const user = new User({
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				login: req.body.login,
				email: req.body.email,
				password: hashedpassword,
				createdAt: new Date().toISOString()
			});

			user.save()
			.then(user => {
				const accessCookie = CreateCookie(user);

				res.cookie(
					"cookie", accessCookie,
					{
						maxAge: 60 * 60 * 24,
						httpOnly: true,
					}
				);
				const { _id, firstname, lastname, login, email, abonne, abonnement, bio, messages } = user;

				res.status(200).json({auth: true, cookie: accessCookie, user: user });
			})
			.catch(err => {
				res.status(500).json({ error: err })
			})
		})
	})
	.catch(err => {
		res.status(500).json({ error: "err"+err })
	});
});

//Login
router.post('/login', async(req, res) => {

	//Validation des données
	const { error } = LoginValide(req.body);
	if(error) return res.status(422).json({ error: error.details[0].message });

	User.findOne({ email: req.body.email })
	.then(user => {
		if(!user) {
			res.status(422).json({ error: "Invalid email or password try again !"})
		}
		bcrypt.compare(req.body.password, user.password)
		.then(doMatch => {
			if(doMatch) {
				const accessCookie = CreateCookie(user);

				res.cookie(
					"cookie", accessCookie,
					{
						maxAge: 60 * 60 * 24,
						httpOnly: true,
					}
				);
				const { _id, firstname, lastname, login, email, abonne, abonnement, bio, messages } = user;

				res.status(200).json({auth: true, cookie: accessCookie, user: user});
			}
		})
	})
});

//Logout
router.post('/logout', async(req, res) => {

	res.cookie('cookie', '', { maxAge: 0 });

	res.send('Vous êtes déconnecté !');

});

module.exports = router;

