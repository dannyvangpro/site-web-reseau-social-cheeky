const { sign, verify } = require("jsonwebtoken");
const User = require("../model/User");

module.exports.CreateCookie = (user) => {
	const accessCookie = sign(
		{ login: user.login, _id: user._id },
		process.env.SECRET_TOKEN
	);

	return accessCookie;
};

module.exports.ValidateCookie = async (req, res, next) => {
	const authCookie = req.headers.authorization;

	if(!authCookie) {
		res.status(401).json({ error: "Pas de cookie, connection refusée" + authCookie });
	}

	const accessCookie = authCookie.split('Bearer ')[1];

	verify(accessCookie, process.env.SECRET_TOKEN, (err, payload) => {
		if(err) {
			res.status(401).json({ error: "Pas connecte" });
		}

		const { _id } = payload;

		User.findById(_id).then(user => {
			req.user = user;
			next();
		})
	});
};