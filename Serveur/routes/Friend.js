const router = require("express").Router();
const ObjectID = require('mongoose').Types.ObjectId;
const User = require("../model/User");
const { ValidateCookie } = require("./Cookie");
const { MessageValide } = require("../Validation");

// Get all the followers
router.get('/all_follower', ValidateCookie, async (req, res) => {

	const id = req.user._id;
	const user = await User.findById(id);

	if(!user) {
		res.status(404).json({ error: "User not found" });
	}
	else {
		const follower = await User.findById(id).select('abonne').select('-_id');
		res.status(200).json( follower );
	}
});

// Get all the following
router.get('/all_following', ValidateCookie, async (req, res) => {

	const id = req.user._id;
	const user = await User.findById(id);

	if(!user) {
		res.status(404).json({ error: "User not found" });
	}
	else {
		const following = await User.findById(id).select('abonnement').select('-_id');
		res.status(200).json( following );
	}
});

// Follow a user
router.patch('/follow', ValidateCookie, async (req, res) => {
	
	//Validation des données
	// const { error } = MessageValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	const id_user = req.user._id;
	const id_user_follow = req.body.userid;

	const user = await User.findById(id_user).select('firstname').select('lastname').select('-_id');
	const user_follow = await User.findById(id_user_follow).select('firstname').select('lastname').select('-_id');

	User.findByIdAndUpdate(
		id_user,
		{
			$addToSet: {
				abonnement: id_user_follow,
			},
		},
		{ 	
			new: true,
			upsert: true
		}
	).then(docs => {
		if(!docs) {
			res.status(404).json({ error: "Cannot add the follow to the user"});
		}
		else {
			res.status(200).json({ message: "The user " + user_follow+" was add to the user " + user + " follow !" });
		}
	}).catch(err => {
		res.status(500).json({ error: err });
	});

	User.findByIdAndUpdate(
		id_user_follow,
		{
			$addToSet: {
				abonne: id_user,
			},
		},
		{ 	
			new: true,
			upsert: true
		}
	).then(docs => {
		if(!docs) {
			res.status(404).json({ error: "Cannot add the user to the follower"});
		}
		else {
			res.status(200).json({ message: "The user " + user +" was add to " + user_unfollow + " followers !"});
		}
	}).catch(err => {
		res.status(500).json({ error: err });
	});
});

// Unfollow a user
router.patch('/unfollow', ValidateCookie, async (req, res) =>  {
	
	//Validation des données
	// const { error } = MessageValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	const id_user = req.user._id;
	const id_user_unfollow = req.body.userid;

	const user = await User.findById(id_user).select('firstname').select('lastname').select('-_id');
	const user_unfollow = await User.findById(id_user_unfollow).select('firstname').select('lastname').select('-_id');

	User.findByIdAndUpdate(
		id_user,
		{
			$pull: {
				abonnement: id_user_unfollow,
			},
		},
		{ 	
			new: true,
			upsert: true
		}
	).then(docs => {
		if(!docs) {
			res.status(404).json({ error: "Cannot remove the follow from the user"});
		}
		else {
			res.status(200).json({ message: "The user " + user_unfollow+" was removed from the user " + user + " followers !" });
		}
	}).catch(err => {
		res.status(500).json({ error: err });
	});

	User.findByIdAndUpdate(
		id_user_unfollow,
		{
			$pull: {
				abonne: id_user,
			},
		},
		{ 	
			new: true,
			upsert: true
		}
	).then(docs => {
		if(!docs) {
			res.status(404).json({ error: "Cannot add the user from the follower"});
		}
		else {
			res.status(200).json({ message: "The user " + user +" was removed from " + user_unfollow + " follow ! " });
		}
	}).catch(err => {
		res.status(500).json({ error: err });
	});
});

// Search
router.get("/search",ValidateCookie, async (req,res) => {
	User.findOne({ firstname: req.body.firstname }).select('-password')
	.then(user => {
		if(!user) {
			User.findOne({ lastname: req.body.lastname }).select('-password')
			.then(user => {
				if(!user) {
					User.findOne({ login: req.body.login }).select('-password')
					.then(user => {
						if(!user) {
							res.status(404).json({error : "User not found " });
						}
						else{
							res.status(200).json(user);
						}
					})
					.catch(err => {
						res.status(404).json({error : "User not found " })
					})
				}
				else{
					res.status(200).json(user);
				}
			})
			.catch(err => {
				res.status(404).json({error : "User not found " });
			})
		}
		else {
			res.status(200).json(user);
		}
	})
	.catch(err => {
		res.status(404).json({error : "User not found " });
	})
});

// Create a new message
router.post('/message', ValidateCookie, async (req, res) => {
	
	//Validation des données
	// const { error } = MessageValide(req.body);
	// if(error) return res.status(400).json({ error: error.details[0].message });

	User.findByIdAndUpdate(
		req.body.id,
		{
			$push: {
				messages: {
					author: req.user,
					message: req.body.message,
					timestamp: new Date().getTime()
				}
			},
		},
		{ 
			new: true
		}
	).then(message => {
		if(!message) {
			res.status(404).json({ error: "Cannot create a message " + message });
		}
		else {
			res.status(200).json({ message: "The message was successfully sent !" + message });
		}
	}).catch(err => {
		res.status(500).json({ error: "Error creating the message !" + err });
	});
});

// Get all the message
router.get('/all_messages', ValidateCookie, (req, res) => {
	
	User.find()
	.select( 'messages' )
	.sort( '-createdAt' )
	.then(message => {
		if(!message) {
			res.status.json({ errors: "Error to get all the messages" });
		}
		else {
			res.json( message );
		}
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the messages" });
	});
});

// Get all the message of the user
router.get('/all_messages_user', ValidateCookie, (req, res) => {
	
	User.find({ _id: req.user._id })
	.select( 'messages' )
	.sort( '-createdAt' )
	.then(message => {
		if(!message) {
			res.status.json({ errors: "Error to get all the messages" });
		}
		else {
			res.json( message );
		}
	})
	.catch(err => {
		res.status(500).json({ error: "Error finding the messages" });
	});
});

// Delete a message
router.delete('/delete', ValidateCookie, async (req, res) => {
	
	User.findById(
		req.user._id,
		{
			$pull: {
				messages: {
					_id: req.body.messsageId
				}
			}
		},
		{
			new: true
		}
	).then(message => {
		if(!message) {
			res.status(404).send({ error: "Cannot delete the message " + message });
		}
		else {
			res.send({ message: "The message " + message + " was deleted successfully !" });
		}
	})
	.catch(err => {
		res.status(500).send({ error: "Error finding and deleting the message !" });
	});
});

module.exports = router;